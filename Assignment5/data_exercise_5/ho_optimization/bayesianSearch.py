import sys
import os

cwd = os.getcwd()
sys.path.append(cwd)

robo_dir = cwd + '/RoBO-master'
sys.path.append(robo_dir)

import ho_exercise as h
import numpy as np
import matplotlib.pyplot as plt
import randomSearch as rs

from robo.fmin import bayesian_optimization

def main(iterations=10, samples=50):
    lower_bounds = np.array([-6,  32,  4,  4,  4])
    upper_bounds = np.array([ 0, 512, 10, 10, 10])
    
    result = np.zeros(samples)
    temp_result = None

    for i in range(iterations):
        temp_result = bayesian_optimization(h.objective_function, lower_bounds,
                                            upper_bounds, num_iterations=samples)
        result = np.add(result, temp_result["incumbent_values"])
        # print(temp_result['incumbent_values'])
    
    return np.multiply(result, 1/iterations), temp_result['X']


def plotImage(histogram_bayesian, histogram_random, name):
    plt.plot(histogram_bayesian, linewidth=2.0, label='bayesian')
    plt.plot(histogram_random, linewidth=2.0, label='random')
    plt.savefig('/home/philipp/deeplearninglab/Assignment5/' + name + '.png')
    plt.close()


if __name__ == '__main__':
    histogram, hyperparams = main(iterations=10, samples=50)
    histogram2, hyperparams2  = rs.main(iterations=10, samples=50)
    
    runtime1 = [h.runtime(param) for param in hyperparams]
    runtime1 = np.cumsum(runtime1)
    runtime2 = [h.runtime(param) for param in hyperparams2]
    runtime2 = np.cumsum(runtime2)
    
    plotImage(histogram, histogram2, 'histogram')
    plotImage(runtime1, runtime2, 'runtime')
