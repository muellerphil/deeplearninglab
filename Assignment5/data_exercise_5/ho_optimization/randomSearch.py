import sys
import os
sys.path.append(os.getcwd())

import ho_exercise as h
import numpy as np
import matplotlib.pyplot as plt

def computeAveragedSearchOptimization(iterations=10, samples=50):
    result_histogram = np.zeros(samples)
    hyper_params = None
    for i in range(iterations):
        histogram, min_config, min_costs = makeRandomSearchOptimization(samples)
        np_histogram = np.array(histogram)[:,1]
        hyper_params = np.array(histogram)[:,0]
        result_histogram = np.add(result_histogram, np_histogram)

    return np.dot(result_histogram, 1 / iterations), hyper_params

def makeRandomSearchOptimization(samples):
    configs_histogram = []
    min_config = None
    min_costs = None

    for i in range(samples):
        config = getRandomConfiguration()
        while configAlreadyUsed(configs_histogram, config):
            print('alreadyused')
            config = getRandomConfiguration()
            print('new one: ' + str(config))

        current_costs = h.objective_function(config)
        if min_config is None:
            min_config = config
            min_costs = current_costs
        else:
            if current_costs < min_costs:
                min_config = config
                min_costs = current_costs
        configs_histogram.append([min_config, min_costs])

    return configs_histogram, min_config, min_costs

def getRandomConfiguration():
    """ Returns a random uniform distributed configuration """
    learning_rate = np.random.randint(-6, 0, dtype='int')
    batch_size = np.random.randint(32, 512, dtype='int')
    num_filter_1 = np.random.randint(4, 10, dtype='int')
    num_filter_2 = np.random.randint(4, 10, dtype='int')
    num_filter_3 = np.random.randint(4, 10, dtype='int')

    return [learning_rate, batch_size, num_filter_1, num_filter_2, num_filter_3]

def configAlreadyUsed(used_configs, current_config):
    for i in range(len(used_configs)):
        if used_configs[i][0] == current_config:
            return True
    return False

def plotImage(histogram):
    plt.plot(np.array(range(len(histogram))), histogram, linewidth=2.0)
    plt.axis([0, len(histogram), 0, 1])
    plt.show()

def main(iterations=10, samples=50):
    num_samples = 100 if samples is None else samples
    iterations = 50 if iterations is None else iterations
    return computeAveragedSearchOptimization(iterations, num_samples)
    # histogram = computeAveragedSearchOptimization(iterations, num_samples)
    plotImage(histogram)

if __name__ == "__main__":
    main()
