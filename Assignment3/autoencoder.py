import sys
sys.path.append('C:/Users/Philipp Müller/Documents/Studium/Fächer/'
                +'DeepLearningLab/deeplearninglab/Assignment3')

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import helperfunctions as hf


def neuralNetwork(inputs):
    return decoder(encoder(inputs))

def encoder(input):
    with tf.name_scope('reshape'):
        x_ = tf.reshape(input, [-1, 28, 28, 1])

    with tf.name_scope('conv_layer_1'):
        conv_1_W = hf.weight_variable([3, 3, 1, 8])
        conv_1_b = hf.bias_variable([8])
        conv_1_h = tf.nn.leaky_relu(hf.conv2d(x_, conv_1_W) + conv_1_b)

    with tf.name_scope('max_pooling_1'):
        pool_1_h = hf.max_pool_2x2(conv_1_h)

    with tf.name_scope('conv_layer_2'):
        conv_2_W = hf.weight_variable([3, 3, 8, 4])
        conv_2_b = hf.bias_variable([4])
        conv_2_h = tf.nn.leaky_relu(hf.conv2d(pool_1_h, conv_2_W) + conv_2_b)

    with tf.name_scope('max_pooling_2'):
        pool_2_h = hf.max_pool_2x2(conv_2_h)

    with tf.name_scope('conv_layer_3'):
        conv_3_W = hf.weight_variable([1, 1, 4, 2])
        conv_3_b = hf.bias_variable([2])
        conv_3_h = hf.conv2d(pool_2_h, conv_3_W) + conv_3_b

    return conv_3_h

def decoder(inputs):
    with tf.name_scope('conv_layer_transposed_1'):
        conv_trans_1_h = hf.conv2d_t(inputs=inputs, filters=4)

    with tf.name_scope('conv_layer_4'):
        conv_4_W = hf.weight_variable([3, 3, 4, 4])
        conv_4_b = hf.bias_variable([4])
        conv_4_h = tf.nn.leaky_relu(hf.conv2d(conv_trans_1_h, conv_4_W) + conv_4_b)

    with tf.name_scope('conv_layer_transposed_2'):
        conv_trans_2_h = hf.conv2d_t(inputs=conv_4_h, filters=8)

    with tf.name_scope('conv_layer_5'):
        conv_5_W = hf.weight_variable([3, 3, 8, 8])
        conv_5_b = hf.bias_variable([8])
        conv_5_h = tf.nn.leaky_relu(hf.conv2d(conv_trans_2_h, conv_5_W) + conv_5_b)

    with tf.name_scope('conv_layer_6'):
        # TODO: Finde heraus, warum hier 3,3 nur funktioniert und nicht 1, 1
        conv_6_W = hf.weight_variable([3, 3, 8, 1])
        conv_6_b = hf.bias_variable([1])
        conv_6_h = tf.nn.leaky_relu(hf.conv2d(conv_5_h, conv_6_W) + conv_6_b)

    return conv_6_h

def main(learning_rate=0.001, max_epoches=20000, num_print_images=3,
         saveOrRestore='save', noiseFactor=0):

    mnist = hf.importMNIST()
    save_dir = 'C:/Users/plotValuesExercise3/' + str(learning_rate)
    save_path_model = save_dir + '/savedSession/model.ckpt'

    # Placeholder for input and loss
    x  = tf.placeholder(tf.float32, [None, 28*28])

    # Build the graph
    y_ = neuralNetwork(x)
    y_flat = tf.reshape(y_, [-1, 28*28])

    with tf.name_scope('loss'):
        loss = tf.reduce_mean(tf.square(y_flat - x))
        tf.summary.scalar('loss', loss)

    with tf.name_scope('AdamOptimizer'):
        train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss)

    merged = tf.summary.merge_all()
    startTime = hf.startSequence(learning_rate)

    saver = tf.train.Saver()

    with tf.Session() as sess:

        writer = tf.summary.FileWriter(save_dir, graph=sess.graph)
        if saveOrRestore is 'save':
            sess.run(tf.global_variables_initializer())
            for i in range(max_epoches):
                batch_train = mnist.train.next_batch(64)
                batch_train = [hf.putNoiseOn(image, noiseFactor) for image in batch_train]
                if i % 5000 == 0:
                    loss_valid = hf.safePredictionOfValidationLoss(mnist, loss, x, noiseFactor)
                    print('step {}, loss on validation data: {:.5f} '.format(
                          i, loss_valid), end='\n')

                train_step.run(feed_dict={x: batch_train[0]})
                summary_str = sess.run(merged, feed_dict={x: batch_train[0]})
                writer.add_summary(summary_str, i)

            # save the session
            save_path = saver.save(sess, save_path_model)
            print('Saved model to directory: {}'.format(save_path_model))

        elif saveOrRestore is 'restore':
            saver.restore(sess, save_path_model)
        else:
            raise ValueError('Wrong input for variable saveOrRestore!,\n'
                              'Correct values are \"safe\" or \"restore\"')

        loss_valid = hf.stopSequence(startTime, mnist, loss, x, noiseFactor)
        hf.printPictures(num_print_images, save_dir, mnist, x, y_, noiseFactor)
        return loss_valid


if __name__ == "__main__":

    learning_rates = [0.001, 0.01, 0.099]
    loss = [0, 0, 0]
    loss_with_noise = [0,0,0]
    for i, lR in enumerate(learning_rates):
        loss[i] = main(learning_rate=lR, max_epoches=10000,
                       num_print_images=3, saveOrRestore='save', noiseFactor=0)
        print('The loss for learningrate {} without noise is {:.5}'.format(lR, loss[i]), end='\n')

        tf.reset_default_graph()

        loss_with_noise[i] = main(learning_rate=lR, max_epoches=20000,
                                  num_print_images=3, saveOrRestore='restore', noiseFactor=0.6)
        print('The loss for learning rate {} with noise is {:.5}'.format(lR, loss_with_noise[i]), end='\n')

        tf.reset_default_graph()
