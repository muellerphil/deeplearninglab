import tensorflow as tf
import numpy as np

from time import time
from tensorflow.examples.tutorials.mnist import input_data
from matplotlib import pyplot as plt


def startSequence(learning_rate):
    print(80 * '=',
    'Starting Training...',
    '  Learning rate: {}'.format(learning_rate),
    #'  Number of filters {}'.format(num_filter),
    #'  Total amount of parameter {}'.format(countParameters()),
    sep='\n')
    return time()

def stopSequence(startTime, mnist, loss, x, noiseFactor):
    duration = time() - startTime
    loss_valid = safePredictionOfValidationLoss(mnist, loss, x, noiseFactor)
    print(80*'=',
    'Training ended',
    '  Duration: {:.4f} s'.format(duration),
    '  Accuracy on validation data {:.4f} %'
     .format(loss_valid),
    sep='\n', end='\n\n')
    return loss_valid

def importMNIST():
    print(80 * '=', 80 * '=', 'Import data...', sep='\n')
    return input_data.read_data_sets('/tmp/tensorflow/mnist/input_data',
                                     one_hot=True)

def printPictures(num_print_images, save_dir, mnist, x, y_, noiseFactor):
    for i in range(num_print_images):
        # print original picture
        noise_str = 'withNoise' if noiseFactor != 0 else 'noNoise'
        name = '{}-{}-{}-origin.png'.format(save_dir, noise_str, i)
        label = [mnist.validation.next_batch(1)[0][0],]
        image = putNoiseOn(np.array(label).reshape((28,28)), noiseFactor)
        plt.title('Written letter')
        plt.imsave(name, image, cmap='gray')

        # print autoencoded picture
        name = '{}-{}-{}-prediction.png'.format(save_dir, noise_str, i)
        autoencoded = y_.eval(feed_dict={x: label})
        autoencoded_image = np.array(autoencoded).reshape((28,28))
        plt.title('autoencoded image')
        plt.imsave(name, autoencoded_image, cmap='gray')

def putNoiseOn(image, noiseFactor):
    return  np.clip((image + noiseFactor * np.random.normal(loc=0.0, scale=1.0, size=image.shape)), 0., 1.)

# initalizing the weight matrices with a standard normal distribution
def weight_variable(shape):
    initial  = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

# Initalizing the bias vector with a constant values
def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

# Creating a convolutional Layer with stride = 1 and no padding.
def conv2d(inputs, filters):
    return tf.nn.conv2d(input=inputs, filter=filters, strides=[1, 1, 1, 1],
                        padding='SAME')

def conv2d_t(inputs, filters):
    return tf.layers.conv2d_transpose(inputs=inputs, filters=filters,
                                      kernel_size=[2, 2], strides=[2, 2],
                                      padding='SAME')

# Creating a maximum pooling layer with a pool size of 2x2
def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1],
                          padding='SAME')


def buildLayer(last_input, weight_shape, bias_shape, layerType, activation_fun=None):
    """ Creating different layerTypes.
    Args:
        last_input(tensor):     the input of the layer (e.g mnist-images)
        weight_shape([int]):    list containing the shape of the weight
                                the last to Dimensions are the filter size
        bias_shape([int]):      list containing the shape of the bias
        layerType(str):         'conv'   for convolutional layer
                                'conv_t' for transposed convolutional layer
                                'fc'     for fully connected layer
        activation_fun:         a tensorflow activation function
    Returns:
        (Weights W, biases B, output h)
    """
    W = weight_variable(weight_shape)
    B = bias_variable(bias_shape)
    h = None

    if layerType is 'conv':
        h = (activation_fun(conv2d(last_input, W) + B)
                 if activation_fun is not None
                 else conv2d(last_input, W) + B)
    elif layerType is 'conv_t':
        h = (activation_fun(conv2d_t(last_input, W) + B)
                if activation_fun is not None
                else conv2d_t(last_input, W) + B)
    elif layerType is 'fc':
        h = (activation_fun(tf.matmul(last_input, W) + B)
                 if activation_fun is not None
                 else tf.matmul(last_input, W) + B)
    else:
        raise ValueError('Wrong layerType')

    return W, B, h

# Compute the Validation loss by spliting the whole validation data set in multiple batches
# This avoids a resource exhausted error.
def safePredictionOfValidationLoss(validation_data, accuracy, x,  noiseFactor, batch_size=64):
    batch_num = int(validation_data.validation.num_examples / batch_size)
    test_accuracy = 0

    for i in range(batch_num):
        batch = validation_data.validation.next_batch(batch_size)
        batch = [putNoiseOn(image, noiseFactor) for image in batch]
        test_accuracy += accuracy.eval(feed_dict={x: batch[0]})

    test_accuracy /= batch_num
    return test_accuracy
